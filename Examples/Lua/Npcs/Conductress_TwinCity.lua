NpcId = 10063 -- This must be the first line, this will be used by the server to get the Npc this script belongs to

function NpcInteract()
    Dialog:Avatar(1)
    Dialog:Dialog("Where are you heading for? I can teleport you for a price of 100~silver.")
    Dialog:Option("Phoenix Castle", PhoenixCastle)
    Dialog:Option("Desert City", DesertCity)
    Dialog:Option("Ape Mountain", ApeMountain)
    Dialog:Option("Bird Island", BirdIsland)
    Dialog:Option("Mine Cave", MineCave)
    Dialog:Option("Market", Market)
    Dialog:JustPassingBy()
end

function HasMoney(amount)
    if (Client.Gold >= amount) then
        Client.Gold = Client.Gold - amount
        return true
    else
        Dialog:Avatar(1)
        Dialog:Dialog("Sorry you do not have " .. amount .. " silver.")
        Dialog:ISee()
        return false
    end
end


function Market(text)
    if (Client.IsBlackName) then
        Dialog:Avatar(1)
        Dialog:Dialog("You are a criminal, it will cost you 1,000 silver to enter the market. Would you like to enter?")
        Dialog:Option("Yes please.", EnterMarketCriminal)
        Dialog:JustPassingBy()
    else
        if (false == HasMoney(100)) then
            return true -- Breaks out of the function
        end
        Client:SetRecordLocation(1002, 301, 279)
        Client:Teleport(1036, 216, 196, true)
    end
end

function EnterMarketCriminal(text)
    if (false == HasMoney(1000)) then
        return true -- Breaks out of the function
    end
    Client:SetRecordLocation(1002, 301, 279)
    Client:Teleport(1036, 216, 196, true)
end

function PhoenixCastle(text)
    if (false == HasMoney(1000)) then
        return true -- Breaks out of the function
    end
    Client:Teleport(1002, 102, 359, true)
end

function MineCave(text)
    if (false == HasMoney(100)) then
        return true -- Breaks out of the function
    end
    Client:Teleport(1002, 102, 359, true)
end

function BirdIsland(text)
    if (false == HasMoney(100)) then
        return true -- Breaks out of the function
    end
    Client:Teleport(1002,525,331,true);
end

function ApeMountain(text)
    if (false == HasMoney(100)) then
        return true -- Breaks out of the function
    end
    Client:Teleport(1002,523,808,true);
end


function DesertCity(text)
    if (false == HasMoney(100)) then
        return true
    end
    Client:Teleport(1002,72,350,true);
end

function PhoenixCastle(text)
    if (false == HasMoney(100)) then
        return true -- Breaks out of the function
    end
    Client:Teleport(1002,809,521,true);
end

