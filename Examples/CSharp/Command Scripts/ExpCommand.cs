using LegacyConquer.Api;
using LegacyConquer.Api.Entity;
public class ExpCommand : GameCommand
{
    public override string Command => "awardexp";
    public override int AdminLevel => 2;
    public override void Execute(IGameClient client, params string[] arguments)
    {
       if (long.TryParse(arguments[0],out var exp))
       {
           client.Exp.AwardExp(exp,out _);
           client.Message($"Your exp is now at {client.Exp.Percent:N3} Percent");
       }
    }
}

