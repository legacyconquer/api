using LegacyConquer.Api;
using LegacyConquer.Api.Entity;

public class ScrollCommand : GameCommand
{
    public override string Command => "scroll";

    public override int AdminLevel => 1;

    public override void Execute(IGameClient client, params string[] arguments)
    {
        switch (arguments[0])
        {
            case "tc":client.Teleport(1002,300,288,true);break;
            case "dc":client.Teleport(1000,494,647,true);break;
            case "ac":client.Teleport(1020,566,565,true);break;
            case "pc":client.Teleport(1011,193,266,true);break;
            case "gs":client.Teleport(3055,61,89,true);break;
            case "bi":client.Teleport(1015,717,577,true);break;
        }
    }
}