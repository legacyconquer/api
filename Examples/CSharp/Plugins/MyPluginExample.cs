﻿using System;
using LegacyConquer.Api;
using LegacyConquer.Api.Entity;
using LegacyConquer.Api.Messages;

namespace ExamplePlugin
{
    public class MyPluginExample:LegacyPlugin
    {
        public override string Name => "Example Plugin";
        public override string Author => "Ultimation";
        public override void Initialize()
        {
        }

        public override void OnClientLogin(IGameClient client)
        {
            client.OnJump += Client_OnJump;
        }

        bool WithinPluginBounds(IGameClient client)
        {
            ushort minX = 400 - 5;
            ushort minY = 400 - 5;
            ushort maxX = 400 + 5;
            ushort maxY = 400 + 5;
            return client.X >= minX && client.X <= maxX && client.Y >= minY && client.Y <= maxY;

        }
        private void Client_OnJump(IGameClient client, ushort x, ushort y)
        {
            Console.WriteLine($"client {client.Name} jumped {x},{y}");
            //Lets test :)
            if (WithinPluginBounds(client))
            {
                var talk = Server.CreateMsg<IMsgTalk>();
                talk.Message = "Really.. i would now kill you :p";
                talk.ChatId = ChatId.Center;
                talk.From = "System";
                client.Send(talk);
                client.Message("I would now kill you :P");
            }
           
        }
    }
}
