using System;
using LegacyConquer.Api.Entity;
using LegacyConquer.Api.Npc;

public class ConductressDesertCity : NpcScript
{
    public override uint NpcId => 10051;

    public override void NpcInteract()
    {
        Dialog.Dialog("Where~are~you~heading~for?~I~can~teleport~you~for~a~price~of~100~silver.");
        Dialog.Option("Twin City",TwinCity);
        Dialog.Option("Market",Market);
        Dialog.JustPassingBy();
        Dialog.Avatar(1);
    }

    bool HasMoney(int amount)
    {
        if (Client.Gold>=amount)
        {
            Client.Gold-=amount;
            return true;
        }
        else
        {
            Dialog.Avatar(1);
            Dialog.Dialog($"Sorry you do not have {amount} silver.");
            Dialog.ISee();
            return false;
        }
    }
    private void Market(string text)
    {
        if (Client.IsBlackName)
        {
            Dialog.Avatar(1);
            Dialog.Dialog("You are a criminal, it will cost you 1,000 silver to enter the market. Would you like to enter?");
            Dialog.Option("Yes please.",EnterMarketCriminal);
            Dialog.JustPassingBy();
        }
        else
        {
            if (!HasMoney(100))
            return;
            Client.SetRecordLocation(1000,502,647);
            Client.Teleport(1036,216,196,true);
        }
    }

    private void EnterMarketCriminal(string text)
    {
         if (!HasMoney(1000))
            return;
             Client.SetRecordLocation(1000,502,647);
            Client.Teleport(1036,216,196,true);
    }

    private void TwinCity(string text)
    {
         if (!HasMoney(100))
            return;
            Client.Teleport(1000,972,666,true);
    }
}