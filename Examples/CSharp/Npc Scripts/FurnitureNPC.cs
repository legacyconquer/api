	using System;
	using System.Collections;
	using LegacyConquer.Api;
	using LegacyConquer.Api.Server;
	using LegacyConquer.Api.Npc;
	using LegacyConquer.Api.Messages;
	using LegacyConquer.Api.Entity;
	/// <summary>
	/// Class generated by the Legacy Conquer TQ Action Converter - Not for unauthorized distribution or use.
	/// Class generated for Npc [30161] FurnitureNPC (CentralPlain)
	/// </summary>
	public class FurnitureNPC_CentralPlain:NpcScript
	{
		public override uint NpcId => 30161;
		
		/// <summary>
		/// The initial npc dialog method
		/// </summary>
		public override void NpcInteract()
		{
			Dialog.Dialog("Welcome!~This~is~the~Twin~City~Furniture~Store.~Take~a~look~around.~I~have~the~best~furniture~in~Twin~City.");
			Dialog.Option("Show~me~your~wares.",Showmeyourwares);
			Dialog.Option("Not~my~concern.",null);
			Dialog.Avatar(188);
		}
		
		/// <summary>
		/// Option Method for Npc Option: 'Show~me~your~wares.'
		/// <param name="text">The input field text</param>
		/// </summary>
		public void Showmeyourwares(string text)
		{
			Client.Teleport(1511,52,70,true);
		}
	}
