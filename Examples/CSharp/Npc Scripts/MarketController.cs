using System;
using LegacyConquer.Api.Entity;
using LegacyConquer.Api.Npc;

public class MarketController : NpcScript
{
    public override uint NpcId => 45;

    public override void NpcInteract()
    {
        Dialog.Avatar(156);
        Dialog.Dialog("Do~you~want~to~leave~the~market?~I~can~teleport~you~for~free.");
        Dialog.Option("Yeah.~Thanks.",LeaveMarket);
        Dialog.Option("No,~I~shall~stay~here.",null);
    }

    private void LeaveMarket(string text)
    {
        Client.ReturnRecordLocation();
    }
}