using System;
using LegacyConquer.Api.Entity;
using LegacyConquer.Api.Npc;

///Conductress for twin city
public class ConductressTwinCity : NpcScript
{
    const uint TwinCityMap=1002;

    
    public override uint NpcId => 10050;

    public override void NpcInteract()
    {
        Dialog.Avatar(1);
        Dialog.Dialog($"Where are you heading for? I can teleport you for a price of 100~silver.");
        Dialog.Option("Phoenix Castle",PhoenixCastle);
        Dialog.Option("Desert City",DesertCity);
        Dialog.Option("Ape Mountain",ApeMountain);
        Dialog.Option("Bird Island",BirdIsland);
        Dialog.Option("Mine Cave",MineCave);
        Dialog.Option("Market",Market);
        Dialog.JustPassingBy();
    }

    bool HasMoney(int amount)
    {
        if (Client.Gold>=amount)
        {
            Client.Gold-=amount;
            return true;
        }
        else
        {
            Dialog.Avatar(1);
            Dialog.Dialog($"Sorry you do not have {amount} silver.");
            Dialog.ISee();
            return false;
        }
    }
    private void Market(string text)
    {
        if (Client.IsBlackName)
        {
            Dialog.Avatar(1);
            Dialog.Dialog("You are a criminal, it will cost you 1,000 silver to enter the market. Would you like to enter?");
            Dialog.Option("Yes please.",EnterMarketCriminal);
            Dialog.JustPassingBy();
        }
        else
        {
            if (!HasMoney(100))
            return;
            Client.SetRecordLocation(TwinCityMap,301,279);
            Client.Teleport(1036,216,196,true);
        }
    }

    private void EnterMarketCriminal(string text)
    {
         if (!HasMoney(1000))
            return;
            Client.SetRecordLocation(TwinCityMap,301,279);
            Client.Teleport(1036,216,196,true);
    }

    private void MineCave(string text)
    {
        if (!HasMoney(100)) return;
         Client.Teleport(TwinCityMap,102,359,true);
    }

    private void BirdIsland(string text)
    {
        if (!HasMoney(100)) return;
        Client.Teleport(TwinCityMap,525,331,true);
    }

    private void ApeMountain(string text)
    {
       if (!HasMoney(100)) return;
        Client.Teleport(TwinCityMap,523,808,true);
    }

    private void DesertCity(string text)
    {
      if (!HasMoney(100)) return;
       Client.Teleport(TwinCityMap,72,350,true);
    }

    private void PhoenixCastle(string text)
    {
       if (!HasMoney(100)) return;
       Client.Teleport(TwinCityMap,809,521,true);
    }
}