# Legacy Conquer API

## Introduction

Here you can find all you need to develop a Command,Plugin or NPC Script for Legacy Conquer.

<i>We support C# & Lua scripts</i>

- Examples Folder -> Here you can find all you need to write your own script ready for Legacy Conquer.

- LegacyConquer.Api.dll -> This will always be kept upto date to expose new api methods and functionality.

- Documentation: Documentation can be found at [https://legacyconquer.gitlab.io/api]

- Help folder -> This is for an internal pipeline and you will probably never need to enter it.

Final notes: We are still in development so the api will change from time to time.

## Script Submission

Scripts can be submitted to our Discord Channel found at [https://discord.gg/9KEjttx]
<br>
Please keep in mind that all scripts will be verified by staff before implementation.
(We don't want you writing scripts to give out free Items,CPS or Gold 😉)